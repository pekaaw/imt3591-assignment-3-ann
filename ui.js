var initUI = function(network)
{
	ui = new UI();
	ui.init(network);
}

function UI() 
{
	var m_learnRate = null;
	var m_nodesInHiddenLayer = null;
	var m_network = null;
	var m_addHiddenLayerButton = null;
	var m_groupingType = null;
	var m_activationFunction = null;
	var m_testLetterValue = null;
	var m_testLetterNumber = null;

	//-CONSTANTS-----------------
	var M_MAXIMUM_LEARN_RATE = 1;
	var M_MINIMUM_LEARN_RATE = 0;
	//---------------------------
	
	this.init = function(network)
	{
		m_network = network;

		m_learnRate = document.getElementById('learningRate');
		m_learnRate.value = 0.3;
		m_learnRate.placeholder = "0->1";
		m_learnRate.addEventListener("keyup", setLearnRate);

		m_network.learningRate = m_learnRate.value;

		m_addHiddenLayerButton = document.getElementById("addHiddenLayer");
		m_addHiddenLayerButton.addEventListener( "click", addHiddenLayer, false );

		m_nodesInHiddenLayer = document.getElementById("nodesInHiddenLayer");
		m_nodesInHiddenLayer.value = 30;
		m_nodesInHiddenLayer.placeholder = "0->*";
		m_nodesInHiddenLayer.addEventListener("keyup", checkValidNode);

		document.getElementById("removeLastHiddenLayer").addEventListener( "click", removeHiddenLayer, false );
		
		m_activationFunction = document.getElementById("activationFunction");
		m_activationFunction.value = "sigmoid";
		m_activationFunction.onchange = setActivationFunction;

		setActivationFunction();

		// Activate buttons for running training and testing epochs
		document.getElementById("runEpochs").addEventListener("click", function() {
			responsiveEpochTraining(document.getElementById("numEpochs").value, m_groupingType);
		});
		document.getElementById("runEpochTests").addEventListener("click", function() {
			responsiveEpochTesting(document.getElementById("numEpochs").value, m_groupingType);
		});

		// recognice what order to run epochs in
		m_groupingType = document.getElementById("groupingSelection").value;
		console.log(m_groupingType);
		document.getElementById("groupingSelection").addEventListener("change", function() {
			m_groupingType = this.value;
			console.log(m_groupingType);
		});

		// test a specific picture in the test-set A1 to Z10
		m_testLetterValue = document.getElementById("testLetterValue").value;
		document.getElementById("testLetterValue").addEventListener("change", function() {
			m_testLetterValue = this.value;
		})
		m_testLetterNumber = document.getElementById("testLetterNumber").value;
		document.getElementById("testLetterNumber").addEventListener("change", function() {
			m_testLetterNumber = this.value;
		})
		document.getElementById("testLetter").addEventListener("click", function() {
			var dataSetID = ( parseInt(m_testLetterValue) * 20 ) + parseInt(m_testLetterNumber)-1 + 10;
			network.feedForward(dataSet[ dataSetID ]);
			// network.backPropagate(Math.floor(dataSetID / 20))
			renderBackground();
			network.draw(ctx);
			document.getElementById("outputCharacter").innerHTML = network.getOutputCharacter();
			document.getElementById("targetCharacter").innerHTML = String.fromCharCode(65 + Math.floor(dataSetID / 20));
			document.getElementById('inputImage').src = imageHandles[dataSetID].src;
		}, false );

		document.getElementById("file").addEventListener("change", openFile);
		document.getElementById("save").addEventListener("click", saveToFile);
	}

	var addHiddenLayer = function()
	{
		var lastLayerIndex = m_network.hiddenLayers.length -1;
		var prevLayer = null;

		if( lastLayerIndex >= 0 )
			prevLayer = m_network.hiddenLayers[lastLayerIndex];
		else
			prevLayer = m_network.inputLayer;

		prevLayer.setNumOutputs(m_nodesInHiddenLayer.value);
		
		var hidden = new Layer();
		hidden.init(m_nodesInHiddenLayer.value, 26, activationFunction);
		m_network.hiddenLayers.push(hidden);

		console.log('Added layer with ' + m_nodesInHiddenLayer.value + ' neurons.');

		setActivationFunction();
		renderBackground();
		m_network.draw(ctx);
	}

	var setActivationFunction = function()
	{
		switch(m_activationFunction.value)
		{
			case "sigmoid":
				for( var i = 0; i < m_network.hiddenLayers.length; i++)
				{	
					m_network.hiddenLayers[i].activationFunction = sigmoid;
					m_network.hiddenLayers[i].activationDerivative = sigmoidDerivative;
				}

				m_network.outputLayer.activationFunction = sigmoid;
				m_network.outputLayer.activationDerivative = sigmoidDerivative;
				
				break;
			case "tanh":
				for( var i = 0; i < m_network.hiddenLayers.length; i++)
				{	
					m_network.hiddenLayers[i].activationFunction = hyperbolicTangentActivation;
					m_network.hiddenLayers[i].activationDerivative = hyperbolicTangentDerivative;
				}
				
				m_network.outputLayer.activationFunction = hyperbolicTangentActivation;
				m_network.outputLayer.activationDerivative = hyperbolicTangentDerivative;
				
				break;
		} 
	}

	var removeHiddenLayer = function()
	{
		if(m_network.hiddenLayers.length > 0)
		{
			m_network.hiddenLayers.pop();

			if(m_network.hiddenLayers.length != 0)
				m_network.hiddenLayers[m_network.hiddenLayers.length -1].setNumOutputs(26);
			else
				m_network.inputLayer.setNumOutputs(26);
			
			renderBackground();
			m_network.draw(ctx);
			console.log("Removed hidden layer");
		}

	}

	var setLearnRate = function()
	{
		if(m_learnRate.value <= M_MAXIMUM_LEARN_RATE && m_learnRate.value >= M_MINIMUM_LEARN_RATE)
			m_network.learningRate = m_learnRate.value
		else
		{
			console.log("Not a valid number");
			m_learnRate.value = 0.3;
			m_network.learningRate = m_learnRate.value;
		}
	}
	var checkValidNode = function()
	{
		if(m_nodesInHiddenLayer.value < 0)
		{
			console.log("Nodes must be more than zero");
			m_nodesInHiddenLayer.value = 1;
		}
	}

	var openFile = function(event) {
		var file = event.target.files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
			var data = JSON.parse(e.target.result);
			if (data.hiddenlayers) {
				network.hiddenLayers = [];
				var previousLayer = network.inputLayer;
				for (var i = 0; i < data.hiddenlayers.length; i++) {
					var layer = new Layer();
					layer.init(data.hiddenlayers[i], network.outputLayer.neurons.length - 1, null)
					previousLayer.setNumOutputs(data.hiddenlayers[i]);
					network.hiddenLayers.push(layer);
					previousLayer = layer;
				}
				setActivationFunction();
			}
			network.learningRate = data.learningrate || 0.3;
			network.errorLimit = data.errorlimit || 0.9;
			if (data.weights) {
				network.inputLayer.setWeights(data.weights[0]);
				for (var i = 0; i < network.hiddenLayers.length; i++) {
					network.hiddenLayers[i].setWeights(data.weights[i + 1]);
				}
			}
		};
		reader.readAsText(file);
	}

	function saveToFile() {
		var hiddenlayers = [];
		for (var i = 0; i < network.hiddenLayers.length; i++) {
			hiddenlayers.push(network.hiddenLayers[i].neurons.length - 1);
		}
		var weights = [];
		weights.push(network.inputLayer.getWeights());
		for (var i = 0; i < network.hiddenLayers.length; i++) {
			weights.push(network.hiddenLayers[i].getWeights());
		}

		var data = {
			"patterns":26,
			"hiddenlayers":hiddenlayers,
			"learningrate":network.learningRate,
			"errorlimit":network.errorLimit,
			"weights":weights
		};

		blob = new Blob([JSON.stringify(data)], {type: "application/json"});
		saveAs(blob, "ann_data.json");
	}
}

var ui = null;

