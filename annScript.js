/* annScript */

/* ann():
	ann-related variables
	function renderBackground();
	function render();
	var onResize = function()
	function updateCharacter( input )
	var paint = { circle: function( context, x, y, radius, color ) ..}
	function retrieveImage( fileName, outputArray )
		>function getPixelInvertedGreyValue( gPIGV_pixelValues, gPIGV_x, gPIGV_y )
	function loadDataSet()
	function init()
	function showPendingOP( startOnFinish )
	function run()
	function feedForward()
	
	Decitions and explanations:
		> The dataSet array has an entry for each input-image. Such an entry contain an array
			filled with average values of 3x2 pixels in the image. With images of size 60 x 38
			it will be needed (60*38)/6 fields of 3x2 pixels. That gives us 380 values for each
			'3x2 pixels' for each image. This value span from 0 to 1.
		> Values to the input-layer will span from 0 to 1. I suggest having a treshold function
			in these nodes so that the network will be responsible for interpreting the input.
*/

// the (activation-) function to use in a neuron (here in hidden- and output-layer, a value between 1 and 0)
var sigmoid = function( f_input ) 
{
	return ( 1 / ( 1 + Math.exp( - f_input) ) );
}

var sigmoidDerivative = function(output)
{
	return( output * (1 - output) );
}

var hyperbolicTangentActivation = function( f_input )
{
    return( (Math.exp(2 * f_input) - 1) / (Math.exp(2 * f_input) + 1) );
}

var hyperbolicTangentDerivative = function( f_input )
{
	return(1.0 - f_input * f_input);
}



// an activation-function to use in a neuron, a threshold function
var threshold_twenty = function( f_input ) {
	if( f_input < 0.2 ) {				// set 20% as a threshold
		return 0;
	} else {
		return 1;
	}
}

// Activation function does nothing.
var activationNoChange = function(f_input) {
	return f_input;
}

function Neuron(numOutputs, indexInLayer) {
	this.indexInLayer = indexInLayer;
	this.outputValue = 0;
	this.outputWeights = new Array();
	this.errorValue = 0;
	this.targetOutput = 0;

	this.initOutputWeights = function(num_outputs)
	{
		this.outputWeights = [];

		for (var i = 0; i < num_outputs; i++) 
			this.outputWeights.push(Math.random() * 2 - 1);
	}

	this.initOutputWeights(numOutputs);

	// Calculates the net value from previousLayer and passes it threough the activation function.
	this.feedForward = function(previousLayer, activationFunction) {
		var netValue = 0;
		for (var i = 0; i < previousLayer.neurons.length; i++) {
			var neuronOutput = previousLayer.neurons[i].outputValue;
			var neuronWeight = previousLayer.neurons[i].outputWeights[this.indexInLayer];
			netValue += neuronOutput * neuronWeight;
		}
		this.outputValue = activationFunction(netValue);
	}

	// Calculates how much error this neuron created for nextLayer.
	this.calculateError = function(nextLayer) {
		var error = 0;
		for (var i = 0; i < nextLayer.length; i++) {
			error += this.outputWeights[i] * nextLayer.neurons[i].errorValue;
		}
		return error;
	}
}

function Layer() {
	this.neurons = new Array();
	this.activationFunction = null;
	this.activationDerivative = null;
	
	// initialize layer
	this.init = function( num_neurons, num_outputs) {
		// create neurons to fill the neurons-array
		for (var i = 0; i < num_neurons; i++) {
			var neuron = new Neuron(num_outputs, i);
			this.neurons.push(neuron);
		}
		// Add a bias to the end of the array.
		var biasNeuron = new Neuron(num_outputs);
		biasNeuron.outputValue = 1;
		this.neurons.push(biasNeuron);
	}

	this.setNumOutputs = function(num_outputs)
	{
		for( var i = 0; i < this.neurons.length; i++)
		{
			this.neurons[i].initOutputWeights(num_outputs);
		}
	}

	// Set the output data of the layer to outputArray. Used to set input data set.
	this.setOutputValues = function(outputValues) {
		if (outputValues.length != this.neurons.length - 1) {
			alert("Output value array invalid. (" + outputValues.length + " != " + (this.neurons.length - 1) + ")");
		}
		for( var i = 0; i < outputValues.length; i++ ) {
			this.neurons[i].outputValue = outputValues[i];
			// console.log('Output of neuron ' + i + ' set to ' + outputValues[i]);	
		}
	}

	// Feed forward from previousLayer.
	this.feedForward = function(previousLayer) {
		// Feed forward to all neurons except for the bias neuron which already has its output set.
		for (var i = 0; i < this.neurons.length - 1; i++) {
			this.neurons[i].feedForward(previousLayer, this.activationFunction);
			// console.log('Output of neuron ' + i + ' set to ' + this.neurons[i].outputValue);
		}
	}

	// Find the error for each neuron in the output-layer
	this.calculateResultError = function() {
		var number_of_neurons = this.neurons.length;
		for( var i = 0; i < number_of_neurons; i++ ) {
			var target = this.neurons[i].targetOutput;
			var output = this.neurons[i].outputValue;
			var error = target - output;

			this.neurons[i].errorValue = error * this.activationDerivative(output);
			// console.log(this.neurons[i].errorValue)
		}
	}

	// Backpropagate from nextLayer.
	this.backPropagate = function(nextLayer) {
		for( var i = 0; i < this.neurons.length; i++ ) {
			var error = this.neurons[i].calculateError(nextLayer);
			var output = this.neurons[i].outputValue;
			this.neurons[i].errorValue = error * this.activationDerivative(output);
		}
	}
	
	// Set the targetOutput in all neurons to 0,
	// then set it to 1 in the neuron with targetIndex 
	this.setTargetOutput = function( targetIndex ) {
		var number_of_neurons = this.neurons.length;
		for( var i = 0; i < number_of_neurons; i++ ) {
			this.neurons[i].targetOutput = 0;
		}
		this.neurons[targetIndex].targetOutput = 1;
	}
	
	// Update weights according to error found
	this.updateWeights = function(nextLayer, learningRate) {
		// for each neuron in this layer
		for( var i = 0; i < this.neurons.length; i++ ) {
			// for each weight in this neuron
			for( var j = 0; j < this.neurons[i].outputWeights.length; j++ ) {
				// apply delta rule
				var deltaWeight = learningRate * this.neurons[i].outputValue * nextLayer.neurons[j].errorValue;
				// add delta-value to the weight
				this.neurons[i].outputWeights[j] += deltaWeight;
			}
		}
	}
	
	// Draw a visual representation of the output in this layer
	this.drawOutput = function( posX, posY , ctx) {
		var inData = document.createElement("canvas");
		var inDataCtx = inData.getContext("2d");
		// find height and width with a factor and a size (quadratic equation)
		var factor = 19/20;
		var top = Math.sqrt( -4*factor*-this.neurons.length ); // sqrt(-4ac)
		if( isNaN(top) ) {
			return false;
		}
		var sideWidth = top / (2*factor);
		var sideHeight = sideWidth * factor;
		inData.width = sideWidth;
		inData.height = sideHeight;
		var inDataImgValues = ctx.createImageData( sideWidth, sideHeight );
		var number_of_neurons = this.neurons.length;
		for( var i = 0; i < number_of_neurons - 1; i++ ) {
			// depends on a value between 0 and 1
			inDataImgValues.data[4*i] = 255 - this.neurons[i].outputValue * 255 ;
			inDataImgValues.data[4*i + 1] = 255 - this.neurons[i].outputValue * 255;
			inDataImgValues.data[4*i + 2] = 255 - this.neurons[i].outputValue * 255;
			inDataImgValues.data[4*i + 3] = 255;
		}
		inDataCtx.putImageData( inDataImgValues, 0, 0 );
		ctx.drawImage( inData, posX, posY );
		
		// Draw as bar-chart represantation
		var height = 100;
		var bar_width = 4;
		ctx.save();
		ctx.translate( posX + 30, height+posY );
		ctx.strokeStyle = "#abcdef";
		ctx.lineWidth = bar_width;
		for( var i = 0; i < number_of_neurons - 1; i++ ) {
			ctx.translate( 6, 0 );
			ctx.beginPath();
			ctx.moveTo( 0, 0 );
			ctx.lineTo( 0, -height );
			ctx.stroke();
		}
		ctx.restore();
		ctx.save();
		ctx.translate( posX + 30, height+posY );
		ctx.strokeStyle = "#000000";
		ctx.lineWidth = bar_width;
		for( var i = 0; i < number_of_neurons - 1; i++ ) {
			ctx.translate( 6, 0 );
			ctx.beginPath();
			ctx.moveTo( 0, 0 );
			ctx.lineTo( 0, -this.neurons[i].outputValue * height );
			ctx.stroke();
		}
		ctx.restore();
	}

	this.getWeights = function() {
		var weights = [];
		for (var i = 0; i < this.neurons.length; i++) {
			var nodeWeights = [];
			for (var j = 0; j < this.neurons[i].outputWeights.length; j++) {
				nodeWeights.push(this.neurons[i].outputWeights[j]);
			}
			weights.push(nodeWeights);
		}
		return weights;
	}

	this.setWeights = function(weights) {
		for (var i = 0; i < weights.length; i++) {
			for (var j = 0; j < weights[i].length; j++) {
				this.neurons[i].outputWeights[j] = weights[i][j];
			}
		}
	}
}

function Network() {
	this.outputLayer = null;
	this.inputLayer = null;
	this.hiddenLayers = new Array();

	this.learningRate = null;
	this.errorLimit = 0.9;

	this.getOutputCharacter = function() {
		var highestOutput = 0;
		var highestOutputUnit = null;
		for (var i = 0; i < this.outputLayer.neurons.length - 1; i++) { // Ignore the bias (length - 1).
			currentUnit = this.outputLayer.neurons[i]
			if (currentUnit.outputValue > highestOutput) {
				highestOutput = currentUnit.outputValue;
				highestOutputUnit = currentUnit;
			}
		}

		return String.fromCharCode(65 + highestOutputUnit.indexInLayer);
	}

	this.feedForward = function(data_set) {
		if (this.outputLayer == null) {
			console.log("Error: Network's output layer is not set.");
			return false;
		} else if (this.inputLayer == null) {
			console.log("Error: Network's input layer is not set.");
			return false;
		}

		// put data into input-array and update output values
		// console.log('Feeding data to input layer.');
		this.inputLayer.setOutputValues( data_set );

		var previousLayer = this.inputLayer;
		
		// Go through all the hidden layers.
		for (var index = 0; index < this.hiddenLayers.length; index++) {
			var currentLayer = this.hiddenLayers[index];
			// console.log('Feeding data to hidden layer ' + index + '.');
			currentLayer.feedForward(previousLayer);
			previousLayer = currentLayer;
		}
		
		// get data from hiddenLayer and update output values
		// console.log('Feeding data to output layer.');
		this.outputLayer.feedForward(previousLayer);	// get data
	}

	this.backPropagate = function(targetOutput) {
		this.outputLayer.setTargetOutput( targetOutput );			// set target-output to the networks current target

		// Backpropagate
		// console.log('Calculating error in output neurons.');
		this.outputLayer.calculateResultError();					// Find error in output-neurons

		var nextLayer = this.outputLayer;
		for (var index = this.hiddenLayers.length - 1; index >= 0; index--) {
			var previousLayer = this.hiddenLayers[index];
			// console.log('Backpropagating to hidden layer ' + index + '.');
			previousLayer.backPropagate(nextLayer);
			nextLayer = previousLayer;
		}
		// console.log('Backpropagating to input layer.');
		this.inputLayer.backPropagate(nextLayer);

		// Update all weights.
		var nextLayer = this.outputLayer;
		for (var index = this.hiddenLayers.length - 1; index >= 0; index--) {
			// console.log('Updating hidden layer ' + index + ' weights.');
			var previousLayer = this.hiddenLayers[index];
			previousLayer.updateWeights(nextLayer, this.learningRate);
			nextLayer = previousLayer;
		}
		// console.log('Updating input layer weights.');
		this.inputLayer.updateWeights(nextLayer, this.learningRate);

		// this.draw();
	}

	
	// draw output to screen
	this.draw = function(ctx) {
		ctx.save()
		ctx.translate( 0, 25 );
		this.inputLayer.drawOutput(20, 0, ctx);
		for( var i = 0; i < this.hiddenLayers.length; i++ ) {
			this.hiddenLayers[i].drawOutput( 20, 150 * i + 150, ctx );
		}
		this.outputLayer.drawOutput(20, 150 * this.hiddenLayers.length + 150, ctx);
		ctx.restore();
	}
}

var canvas = null;
var ctx = null;

var dataSet = new Array();				// Will contain arrays of inputData from each input image. letters from {A...Z}x20
var imageHandles = new Array();				// Will contain images in img-tags
var trainingSequences = new Array();			// Will contain sequences of indexes from the dataSet. Array( 1 entry = 1 seq )
var pendingOperations = new Array();			// When 0 elements, no pending operations, ready for action
var loadingStatus = "idle";				// Will hold an interval ID or the string "idle" when not in use
var feedCounter = 0;					// Number of image in array imageHandles
var network = null;

function renderBackground() {
	ctx.rect( 0, 0, canvas.width, canvas.height );
	ctx.fillStyle = "#ffff44";
	ctx.fill();
	ctx.beginPath();
	ctx.moveTo( 0, 0 );
	ctx.lineTo( 1, canvas.height-1 );
	ctx.lineTo( canvas.width-1, canvas.height-1 );
	ctx.lineTo( canvas.width-1, 1 );
	ctx.closePath();
	ctx.strokeStyle = "#000000";
	ctx.stroke();
}

function render() {
	renderBackground();
	if (network != null) {
		network.draw(ctx);
	}
}


// Resizes the program to fit the screen.
// (mostly from http://www.html5rocks.com/en/tutorials/casestudies/gopherwoord-studios-resizing-html5-games/)
var onResize = function() {
	var programArea = document.getElementById('programArea');
	var programWindowRatio = 16 / 9;
	var newWidth = window.innerWidth;
	var newHeight = window.innerHeight;
	var newRatio = newWidth / newHeight;

	if (newRatio > programWindowRatio) {
		newWidth = newHeight * programWindowRatio;
	} else {
		newHeight = newWidth / programWindowRatio;
	}

	programArea.style.height = newHeight + 'px';
	programArea.style.width = newWidth + 'px';
	
	// the following depends on programArea to have 0,0 in center of page
	programArea.style.marginTop = (-newHeight / 2) + 'px';
	programArea.style.marginLeft = (-newWidth / 2) + 'px';

	canvas.width = newWidth;
	canvas.height = newHeight;

	render();
}

// Get the next ascii character after the input
function updateCharacter( input ) {
	var output = String.fromCharCode( input.charCodeAt( 0 ) + 1 );
	return output;
}

var paint = {
	//paint.circle( ctx, ctx.canvas.width - 10, 10, 5, "#ff0000" );
	circle: function( context, x, y, radius, color ) {
		context.beginPath();
		context.arc( x, y, radius, 0, 2 * Math.PI, false );
		context.fillStyle = color;
		context.fill();
	}
}


function retrieveImage( fileName, outputArray ) {
	var img = new Image();
	img.onload = function( e ) {
		// upon load, make it manipulable by putting it in a canvas,
		// get the imageData and put average values of blocks of 3x2 into imageInfo,
		// the append these to the output array.
		var imgCanvas = document.createElement("canvas");
		var imgCtx = imgCanvas.getContext("2d");
		imgCanvas.width = img.width;
		imgCanvas.height = img.height;
		var success = imgCtx.drawImage( img, 0, 0 );
		
		var pixelValues = imgCtx.getImageData( 0, 0, img.width, img.height );
		
		function getPixelInvertedGreyValue( gPIGV_pixelValues, gPIGV_x, gPIGV_y ) {
			var sum = 0.0;
			var pixelStart = ( gPIGV_y * ( gPIGV_pixelValues.width * 4 ) ) + ( gPIGV_x * 4 );
			for( var i = 0; i < 3; i++ ) {
				sum += gPIGV_pixelValues.data[ pixelStart + i ];
			}
			var average = sum / 3;				// rgb (we omit the alpha-channel)
			var inverted = 255 - average;		// how much black?
			var unitInterval = inverted / 255;
			return unitInterval;				// Value in the span of {0...1}
		}
		
		// fill imageInfo by calculating average colors in blocks of a picture with
		// a blocksize of 3x2 (WxH). By this we reduce necessary input-nodes in the network
		var imageInfo = new Array();
		var sum = 0.0;
		for( var y = 0; y < img.height; y += 2 ) {
			for( var x = 0; x < img.width; x += 3 ) {
				for( var i = 0; i < 3; i++ ) {
					sum += getPixelInvertedGreyValue( pixelValues, x + i, y );
					sum += getPixelInvertedGreyValue( pixelValues, x + i, y + 1 );
				}
				imageInfo.push( sum / 6 );	// average of 6 pixel in texel
				sum = 0.0;
			}
		}
		
		outputArray.push( imageInfo );
		pendingOperations.splice( 0, 1 );	// Remove first element from pendingOperations
	}
	img.src = fileName;
	return img;
}

//******************************************************************************
// 			Before here is neurons, layers and the network.
//			After here there is the running program
//******************************************************************************


// Start this function when initalization is done (dataset loaded)
function run() {
	var input = new Layer();
	var hidden = new Layer();
	// var hidden2 = new Layer();
	var output = new Layer();
	input.init(dataSet[0].length, dataSet[0].length / 4);
	input.activationFunction = null;
	input.activationDerivative = sigmoidDerivative;

	hidden.init(dataSet[0].length / 4, 26);
	// hidden2.init(dataSet[0].length / 2, 26, sigmoid);
	output.init(26, 0);
	network = new Network();
	network.inputLayer = input;
	network.hiddenLayers.push(hidden);
	// network.hiddenLayers.push(hidden2);
	network.outputLayer = output;
	initUI(network);
}

// execute an epoche. Train with the whole trainingset once.
// This one has responsive UI, but is slower.
var responsiveEpochTraining = function( number_of_epochs, grouping ) {
	var time = new Date();
	var timeStart = time.getTime();
	var mSeconds = null;
	var mSecondsNext = null;
	var duration = null;
	var iteration = 0;
	var busy = false;
	var method = 0;

	switch( grouping ) {
		case "equal": 
			method = 0;
			break;
		case "alphabet":
			method = 1;
			break;
		case "random":
			method = 2;
			break;
		default:
			method = 1;
	}


	var processId = setInterval(
		function() {
			if (!busy) {
				busy = true;

				time = new Date();
				mSeconds = time.getTime();

				if( method == 2 ) {
					createRandomSequence();
				}

				var index;
				var letter;
				// Run through the dataSet in order defined by trainingSequence
				for( var j = 0; j < trainingSequences[method].length; j++ ) {
					// Find the next index and find what letter it represents
					index = trainingSequences[method][j];
					letter = Math.floor( index / 20 );
					// Train the network with the correct data correct weights
					network.feedForward( dataSet[ index ] );
					network.backPropagate( letter );
				}
				
				time = new Date();
				var duration = time.getTime() - mSeconds;
				console.log( "Done with epoch nr. " + iteration + ". in " + duration + " ms." );

				iteration++;
				if (iteration >= number_of_epochs) {
					clearInterval(processId);
					time = new Date();
					duration = time.getTime() - timeStart;
					console.log( "Training with " + number_of_epochs + " epochs was " +
						"done in " + (duration/1000) + "seconds." );
					network.draw(ctx);
				}

				busy = false;
			}
		},
		10
	);
	//function updateCharacter( input )
}

// execute an epoche-test. Test with the whole testset.
// This one has responsive UI, but is slower.
var responsiveEpochTesting = function( number_of_epochs, grouping ) {
	var time = new Date();
	var timeStart = time.getTime();
	var mSeconds = null;
	var mSecondsNext = null;
	var duration = null;
	var iteration = 0;
	var busy = false;
	var method = 0;
	var recognitionSuccesses = 0;

	switch( grouping ) {
		case "equal": 
			method = 0;
			break;
		case "alphabet":
			method = 1;
			break;
		case "random":
			method = 2;
			break;
		default:
			method = 1;
	}


	var processId = setInterval(
		function() {
			if (!busy) {
				busy = true;

				time = new Date();
				mSeconds = time.getTime();

				if( method == 2 ) {
					createRandomSequence();
				}

				var index;
				var letter;
				var answer;
				// Run through the dataSet in order defined by trainingSequence
				for( var j = 0; j < trainingSequences[method].length; j++ ) {
					// Find the next index and find what letter it represents
					// +10 to get the testingset
					index = trainingSequences[method][j] + 10;
					letter = String.fromCharCode(65 + Math.floor( index / 20 ) );
					// Train the network with the correct data correct weights
					network.feedForward( dataSet[ index ] );
					answer = network.getOutputCharacter();
					if( answer == letter ) {
						recognitionSuccesses++;
					}
				}

				network.draw(ctx);

				time = new Date();
				var duration = time.getTime() - mSeconds;
				console.log( "Done with epoch nr. " + iteration + ". in " + duration + " ms." );

				iteration++;
				if (iteration >= number_of_epochs) {
					clearInterval(processId);
					network.draw(ctx);
					// calculate and report success
					var prosentage = recognitionSuccesses / ( trainingSequences[method].length * number_of_epochs );
					var result = document.getElementById("hitResult");
					result.value = prosentage;
					time = new Date();
					duration = time.getTime() - timeStart;
					console.log( "Training with " + number_of_epochs + " epochs was " +
						"done in " + (duration/1000) + "seconds." );
				}

				busy = false;
			}
		},
		10
	);
	//function updateCharacter( input )
}

// Draw all images in a sequence
function feedForward() {
	//console.log( imageHandles[0].height + " height" );
	//console.log( imageHandles[0].width + " width" );
	ctx.drawImage( imageHandles[feedCounter], 20, 10 );

	feedCounter++;
	if( feedCounter == imageHandles.length ) {
		clearInterval( loadingStatus );
		loadingStatus = "idle";
	}
}

function showPendingOP( startOnFinish ) {
	if( pendingOperations.length != 0 ) {
		// if operations are pending (loading data)
		console.log( "In loading: " + pendingOperations.length );
	} else {
		// or when operations done, start function from argument(!)
		console.log( "Loading finished!" );
		clearInterval( loadingStatus );
		loadingStatus = "idle";
		startOnFinish();			// see argument!
	}
}

// Loads the images from A1 to Z20 into dataSet and imageHandles
function loadDataSet() {
	var m_character = "A";
	var m_number = 1;
	var m_file;
	for( var i = 0; i < 26; i++ ) {
		for( var j = 0; j < 20; j++ ) {
			m_file = "character_images/" + m_character + ( m_number + j ) + ".jpg";
			pendingOperations.push( new Date() );
			// put returned image into imageHandles and put testdata into dataSet-array
			imageHandles.push( retrieveImage( m_file, dataSet ) );
			//console.log( m_file );
		}
		m_character = updateCharacter( m_character );
	}
}

// Create different sequences to use when training the network
function createTrainingSequences() {
	// A sequence holds indexes of data/images in the DataSet array.
	// 0th sequence: AAAAAAAAAABBBBBBBBBBCCC...ZZZZZ
	trainingSequences.push( new Array() );
	for( var i = 0; i < 26; i++ ) {
		for( var j = 0; j < 10; j++ ) {
			trainingSequences[0].push( i * 20 + j );
		}
	}

	// 1st sequence: ABCD...XYZABC...XYZ
	trainingSequences.push( new Array() );
	for( var i = 0; i < 10; i++ ) {
		for( var j = 0; j < 26; j++ ) {
			trainingSequences[1].push( j * 20 + i );
		}
	}
	
	// 2nd sequence should be a random sequence. At the beginning we will
	// fill it with the selection, and in use we will switch random positions
	trainingSequences.push( new Array() );
	for( var i = 0; i < 26; i++ ) {
		for( var j = 0; j < 10; j++ ) {
			trainingSequences[2].push( i * 20 + j );
		}
	}
}

function createRandomSequence() {
	// Switch all positions to random places
	var position_one, position_two, temp;
	for( var i = 0; i < trainingSequences[2].length; i++ ) {
		position_one = Math.random() * trainingSequences[2].length;
		position_two = Math.random() * trainingSequences[2].length;
		temp = trainingSequences[2][position_one];
		trainingSequences[2][position_one] = trainingSequences[2][position_two];
		trainingSequences[2][position_two] = temp;
	}		
}

// Initialize the program
function init() {
	canvas = document.getElementById("annCanvas");
	ctx = canvas.getContext('2d');
	// Make sure the canvas is always visible as big as possible on any screen
	window.addEventListener( "resize", onResize, false );
	window.addEventListener( "orientationchange", onResize, false );
	onResize();
	// Start loading of dataset
	loadDataSet();
	// Create sequences to train network in different ways
	createTrainingSequences();
	// Wait for the dataset to be loaded and then continue with the function run
	loadingStatus = setInterval( showPendingOP, 1000, run );
}
